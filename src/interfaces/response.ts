export interface ResponseList<T> {
    rows: T[];
    count: number;
}