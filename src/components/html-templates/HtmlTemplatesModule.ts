import {Module} from '@nestjs/common';

import {HtmlTemplatesService} from './HtmlTemplatesService';

@Module({
    providers: [HtmlTemplatesService],
    exports: [HtmlTemplatesService],
})
export class HtmlTemplatesModule {
}