export interface HtmlTemplate {
    id: number;
    name: string;
    template: string;
    modified: number;
}