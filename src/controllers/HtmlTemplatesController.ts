import {Controller, Get} from '@nestjs/common';

import {HtmlTemplatesService} from '../components/html-templates/HtmlTemplatesService';
import {ResponseList} from '../interfaces/response';
import {HtmlTemplate} from '../interfaces/models/html-template/HtmlTemplateModule';

@Controller('html-templates')
export class HtmlTemplatesController {
    constructor() {
    }

    @Get()
    async findAll(): Promise<ResponseList<HtmlTemplate>> {
        return HtmlTemplatesService.findAll();
    }
}