import {Injectable} from '@nestjs/common';

import {HtmlTemplate} from '../../interfaces/models/html-template/HtmlTemplateModule';
import {HTML_TEMPLATES_LIST} from './tests/mocks';
import {ResponseList} from '../../interfaces/response';

@Injectable()
export class HtmlTemplatesService {

    static findAll(): ResponseList<HtmlTemplate> {

        return {
            rows: HTML_TEMPLATES_LIST,
            count: HTML_TEMPLATES_LIST.length,
        };
    }
}