import {Module} from '@nestjs/common';

import {HtmlTemplatesModule} from './components/html-templates/HtmlTemplatesModule';
import {HtmlTemplatesController} from './controllers/HtmlTemplatesController';

@Module({
    imports: [HtmlTemplatesModule],
    controllers: [HtmlTemplatesController],
})
export class AppModule {
}
